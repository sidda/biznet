﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizNet.Models.Post
{
    public class PostsRepository
    {
        PostsDataContext pdc = new PostsDataContext();
        /// <summary>
        /// Insert new post into database and commits immediately.
        /// </summary>
        /// <param name="p"></param>
        public void insert(Post p)
        {
            pdc.Posts.InsertOnSubmit(p);
            pdc.SubmitChanges();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        public void addComment(Comment c)
        {
            pdc.Comments.InsertOnSubmit(c);
            pdc.SubmitChanges();
        }

        public IList<Post> getAllPosts()
        {
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public IList<Post> getPostByUser(int userid)
        {
            return pdc.Posts.Where(d => d.by == userid).ToList();

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public IList<PostViewModel> getPostWithComments(int userid)
        {
            IList<PostViewModel> possAndComments=new List<PostViewModel>();

            foreach(Post p in pdc.Posts.Where( d => d.by == userid).ToList())
            {
                PostViewModel postView=new PostViewModel();
                postView.post=p;
                postView.comments=(from comment in pdc.Comments 
                                                where comment.to==p.id 
                                                select comment).ToList() ;
                //add postview to list
                possAndComments.Add(postView);
            }
            return possAndComments;
        }
    }

}