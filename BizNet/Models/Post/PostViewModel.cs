﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizNet.Models.Post
{
    public class PostViewModel
    {
        public Post post { get;  set; }
        public IList<Comment>  comments { get;  set; }

    }
}