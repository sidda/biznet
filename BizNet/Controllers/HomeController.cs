﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BizNet.Models.Post;
namespace BizNet.Controllers
{
    public class HomeController : Controller
    {
        PostsRepository postRepo = new PostsRepository(); 
        public ActionResult Index()
        {
            ViewBag.Message = "Hi There! welcome to BizNet!";

            var posts = postRepo.getPostWithComments(1);
            return View(posts);
        }

        public ActionResult You()
        {
            ViewBag.Message = "Hi There! welcome to BizNet!";

            return View();
        }
        
        public ActionResult About()
        {
            return View();
        }
    }
}
