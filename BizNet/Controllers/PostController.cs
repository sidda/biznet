﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BizNet.Models.Post;
using System.IO;
using System.Security.Principal;
namespace BizNet.Controllers
{
    public class PostController : Controller
    {
        //
        // GET: /Post/
        PostsRepository pr = new PostsRepository();
        public string Index()
        {
            return "Hello, world!";
        }

        //
        // GET: /Post/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {

            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        public JsonResult Create(FormCollection collection)
        {
            try
            {

                // TODO: Add insert logic here
                Post p = new Post();
                p.text = collection["text"];

                p.on = DateTime.Now;
                pr.insert(p);

                return Json(new { result = "success" });
            }
            catch
            {
                return Json(new { result = "error" });
            }
        }


        [HttpPost]
        public ActionResult Comment(FormCollection collection)
        {
            Comment c = new Comment();
            c.on = DateTime.Now;
            c.to = Convert.ToInt32(collection["to"]);
            c.text = collection["text"];
            c.by = 1;
            //add comment to database
            pr.addComment(c);
            return Json(new { result = "success" });
        }
        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Post/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult upload(IEnumerable<HttpPostedFileBase> files)
        {
            foreach (var file in files)
            {
                if (file.ContentLength > 0)
                {
                    string filePath = Path.Combine(HttpContext.Server.MapPath("~/Content/Uploads"),
                                                   Path.GetFileName(file.FileName));
                    file.SaveAs(filePath);
                }

            }
            return Json(new { result = "success" });
        }
    }
}
