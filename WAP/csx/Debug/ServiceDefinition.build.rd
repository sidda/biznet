﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="WAP" generation="1" functional="0" release="0" Id="26afd56e-79c3-44ed-a513-b158baf92922" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="WAPGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="BizNet:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/WAP/WAPGroup/LB:BizNet:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="BizNet:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/WAP/WAPGroup/MapBizNet:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="BizNetInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/WAP/WAPGroup/MapBizNetInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:BizNet:Endpoint1">
          <toPorts>
            <inPortMoniker name="/WAP/WAPGroup/BizNet/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapBizNet:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/WAP/WAPGroup/BizNet/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapBizNetInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/WAP/WAPGroup/BizNetInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="BizNet" generation="1" functional="0" release="0" software="C:\Users\sids\Documents\Work\WAP\WAP\csx\Debug\roles\BizNet" entryPoint="base\x86\WaHostBootstrapper.exe" parameters="base\x86\WaIISHost.exe " memIndex="1792" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;BizNet&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;BizNet&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/WAP/WAPGroup/BizNetInstances" />
            <sCSPolicyFaultDomainMoniker name="/WAP/WAPGroup/BizNetFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyFaultDomain name="BizNetFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="BizNetInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="e0d827ea-ae96-41fb-81c9-24f817558102" ref="Microsoft.RedDog.Contract\ServiceContract\WAPContract@ServiceDefinition.build">
      <interfacereferences>
        <interfaceReference Id="3b407f17-8bb8-4a25-b1b5-958dc123284d" ref="Microsoft.RedDog.Contract\Interface\BizNet:Endpoint1@ServiceDefinition.build">
          <inPort>
            <inPortMoniker name="/WAP/WAPGroup/BizNet:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>